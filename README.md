# Vagrant関連

## インストール環境

- Vagrant box list(OS list)
    - CentOS 6.9
    - Debian 8.9.0
    - Ubuntu 16.04 LTS (Xenial Xerus)

## 個人環境の構築方法

- 個人開発環境端末    
    - MacbookAir (macOS Sierra 10.12.6)


1. Vagrantをダウンロードしインストールを行う

    https://www.vagrantup.com/downloads.html

2. VirtualBoxをダウンロードしインストールを行う

    https://www.virtualbox.org/wiki/Downloads

    \> OS X hosts

    ※ Windowsの場合は > Windows hostsをクリック

インストール後、下記コマンドを実行しバージョンが確認されればOKです。
```bash
$ vagrant -v
Vagrant 1.9.8

$ VBoxManage -v
5.1.26r117224
```

任意の場所にvagrant用ディレクトリを作成
```bash
$ mkdir vagrant
$ cd vagrant
```

Vagrantから起動するVirtualBoxの設定ファイルとなるVagrantfileを作成する
```bash
$ vagrant init
$ ls -l
```
Vagrantfileの編集を行う
```bash
$ cp -p Vagrantfile <任意のファイル>
$ ls -l
$ vim Vagrantfile
```
Vagrantfileは以下の例のように編集する
```ruby
Vagrant.configure(2) do |config|
  config.vm.define "controller" do |node|
        node.vm.box = "centos6.9"
        node.vm.hostname = "controller"
        node.vm.network :private_network, ip: "192.168.100.10"
        node.vm.network :forwarded_port, id: "ssh", guest: 22, host: 2210
  end
  config.vm.define "target" do |node|
        node.vm.box = "centos6.9"
        node.vm.hostname = "target"
        node.vm.network :private_network, ip: "192.168.100.20"
        node.vm.network :forwarded_port, id: "ssh", guest: 22, host: 2220
  end
end
```
OSをダウンロードする
```bash
$ vagrant box add centos6.9 https://github.com/CommanderK5/packer-centos-template/releases/download/0.6.9/vagrant-centos-6.9.box
```
ダウンロード後、ダウンロードしたBoxの確認を行う。
```bash
$ vagrant box list
```
OSに関して以下のURLにダウンロードが可能なOSの一覧が記載されているので、別のバージョンを使用したい場合は参照する。

https://github.com/CommanderK5/packer-centos-template/releases/

Vagrantを実行する
```bash
$ vagrant up
```
ターミナルが返って来たらステータスを確認し、実行状態を確認する
runningならOK
```bash
$ vagrant status

Current machine states:

controller               running (virtualbox)
target                    running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```
Vagrantへ接続
```bash
$ vagrant ssh <machine名>

// 例
$ vagrant ssh controller
```
Vagrantの初期パスワードはvagrantです。

Vagrantを停止する
```bash
$ vagrant halt
```

Vagrant BoxのOSをダウンロードする際、Vagrantのアカウントを取得しサイトからダウンロードすると楽

https://app.vagrantup.com/